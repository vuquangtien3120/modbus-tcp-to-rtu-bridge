#include <Arduino.h>
#include <WiFi.h>
#include <ModbusTCP.h>
#include <ModbusRTU.h>

#define REG 0
#define SLAVE_ID 1

ModbusRTU mbRTU;

class ModbusMap : public ModbusTCP {
  public:
  TAddress lastRegister;
  protected:
  TRegister* searchRegister(TAddress addr) override {
    lastRegister = addr; // Save real register requested
    if (addr.isHreg()) // Overrides only Hregs
      return ModbusTCP::searchRegister(HREG(REG)); // Return specific register. Next read or write callback will be called against this register.
    return ModbusTCP::searchRegister(addr);
  }
};

ModbusMap mbTCP;

// Callback function to IP Get Hreg function
uint16_t cbGetHreg(TRegister* reg, uint16_t val) {
  uint16_t res = 0xBEEF;
  if ( !mbRTU.slave() ) {
    mbRTU.readHreg(SLAVE_ID, mbTCP.lastRegister.address, &res); // ReadHreg is called against saved original register not register passed to callback parameter
    while( mbRTU.slave() ) {
      mbRTU.task();
    }
  }
  return res;
}

// Callback function to IP Set Hreg function
uint16_t cbSetHreg(TRegister* reg, uint16_t val) {
  if ( !mbRTU.slave() ) {
    uint16_t res = val;
    mbRTU.writeHreg(SLAVE_ID, mbTCP.lastRegister.address, res);
    while( mbRTU.slave() ) {
      mbRTU.task();
    }
  }
  return val;
}

void setup() {

  Serial.begin(115200);
  Serial1.begin(9600, SERIAL_8N1, 19, 18);
  mbRTU.begin(&Serial1, 17);
  mbRTU.master();


  WiFi.begin("Quang Tien", "31122000");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mbTCP.server();
  mbTCP.addHreg(REG);
  mbTCP.onGetHreg(REG, cbGetHreg);
  mbTCP.onSetHreg(REG, cbSetHreg);
}

void loop() {
  mbTCP.task();
  delay(100);
}